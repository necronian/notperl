#!/usr/bin/env perl

use strict;
use warnings;
no warnings 'recursion';

use Getopt::Long::Descriptive;
use Mail::Internet;
use JSON;

use Data::Dumper;

use YAML qw'LoadFile';

# Take 2 hash refs and return a hash ref to the new joined hash
sub merge {
    my ($hash1, $hash2) = @_;
    my %hash3 = ( %{$hash1}, %{$hash2} );
    return \%hash3;
}

# Recursively flatten a nested array
sub flatten_array { return map { (ref $_  eq "ARRAY") ?  flatten_array(@$_) : $_  } @_; }

# Take a filename to the mail message and return a hash ref to a hash 
# of lowercase header keys and their concatenated content
sub read_headers {
    my $filename = shift;

    open(my $fh, "<", $filename) or die "cannot open < $filename $!";
    my $entity = Mail::Internet->new($fh);
    close($fh);

    my @tags = $entity->head->tags;
    my %headers;

    foreach my $tag (@tags) {
        $entity->head->combine($tag);
        $headers{lc $tag} = $entity->head->get($tag);
    }

    return \%headers;
}

# Get all messages for the notmuch search and return an array of hashes with
# information about the matched messages
sub get_messages {
    my $cfg = shift;

    my $notmuchnew = `notmuch search --format=json --format-version=2 --output=messages $cfg->{search}`;

    my @ids = @{from_json( $notmuchnew )};

    # Split search terms into $batchsize batches to pass at once on the command line
    my @partitioned;
    push @partitioned, [ splice @ids, 0, $cfg->{batchsize} ] while @ids;

    my @messages;
    foreach my $msgs ( @partitioned ) {
        # Create the search string, escape double quotes... they were causing not data to be returned
        my $search_ids = join(' ', map { $_ =~ s/\"/\\\"/g; "id:'" . $_ . "'" } @$msgs );

        my $search_results = `notmuch show --format=json --format-version=2 $search_ids`;

        # Convert the flat array of all returned message hashes to a hash keyed on the message id
        my %hashes = map { ( $_->{id} , $_ ) } flatten_array @{from_json($search_results)};

        push @messages,  grep { ref $_ eq "HASH" } #Only merge hashes, if there was a lookup error it will be ignored
                         map { (defined $hashes{$_}) ? $hashes{$_} : print STDERR $_ . "\n"; } # log problem ids
                         @$msgs;
    }

    return map {merge($_, {"headers"  => read_headers($_->{filename})})} # Read the message and insert the headers
           flatten_array ( @messages );;
}

sub tag_list_id {
    my ($cfg, $taglist, $message) = @_;

    if (defined $message->{headers}->{'list-id'}) {

        # Extract the parts between the <> which is the actual list id
        my $list;
        ($list) = $message->{headers}->{'list-id'} =~ m/.*<(.*)>/g;

        if (! defined $list) {
            $list = $message->{headers}->{'list-id'};
        }

        # If there is a user supplied id -> tag name transform apply it
        if (exists $cfg->{listid}->{transform}->{$list}) {
            $list = $cfg->{listid}->{transform}->{$list};
        }
        
        chomp $list;

        # Add tags for listid->prefix and the prefix / listid from the mail
        push(@$taglist,"+" . $cfg->{listid}->{prefix});
        push(@$taglist,"+" . $cfg->{listid}->{prefix} . "/" . substr($list,0,190-length($cfg->{listid}->{prefix})));

    }

    return $taglist
}

sub tag_folder_info {
    my ($cfg, $taglist, $message) = @_;

    # Get the path of the directory minus the maildir and the account dir
    # This should probably be more generalized for different maildir setups
    my $folder = (split ("/", substr( $message->{filename}, $cfg->{maildir_len})))[1];

    my %blacklist_folders =  map {$_ => 1 } @{$cfg->{folder}->{blacklist}};

    # If the folder is blacklisted don't tag it
    # Also if the folder name is to be transformed don't tag it
    if (! exists $blacklist_folders{$folder} ) {

        # Use tag from folder transform list if this folder is in the list
        if (exists $cfg->{folder}->{transform}->{$folder}) {
            $folder = $cfg->{folder}->{transform}->{$folder};
        }

        push(@$taglist,"+".$folder);
    }

    return $taglist;
}

sub tag_account_info {
    my ($cfg, $taglist, $message) = @_;

    my $account;
    my %blacklist_accounts = map { $_ => 1 } @{$cfg->{account}->{blacklist}};

    # If the x-delivered-to header or delivered-to header exists use that instead
    if (exists $message->{headers}->{'x-delivered-to'}) {
        #Trailing newline
        $account = $message->{headers}->{'x-delivered-to'};
    } else {
        # Get the path of the account minus the maildir
        # This should probably be more generalized for different maildir setups
        $account = (split ("/", substr( $message->{filename}, $cfg->{maildir_len})))[0];
    }

    if (! exists $blacklist_accounts{$account}) {

        if ($cfg->{account}->{transform}->{$account}) {
            $account = $cfg->{account}->{transform}->{$account};
        }

        chomp $account;
        push(@$taglist,"+".$account);
    }

    return $taglist;
}

sub merge_existing_tags {
    my ($cfg, $taglist, $message) = @_;

    my @tags = map {"+".$_} @{$message->{tags}};
    push(@$taglist,@tags);

    return $taglist;
}

sub remove_new_tags {
    my ($cfg, $taglist, $message) = @_;

    my %tags = map { $_ => 1 } @$taglist;

    if (exists $tags{"+$cfg->{newtag}"}) {
        delete $tags{"+$cfg->{newtag}"};
        @$taglist = keys %tags;
        push(@$taglist,"-$cfg->{newtag}");
    }

    return $taglist;
}

sub tag_inbox {
    my ($cfg, $taglist, $message) = @_;

    my %tags = map { $_ => 1 } @$taglist;

    if (! exists $tags{'+kill'} and 
        ! exists $tags{'+spam'} and
        ! exists $tags{"+$cfg->{listid}->{prefix}"} ) {
        push(@$taglist,"+inbox");
    }

    return $taglist;
}

sub encode_tag {
    my $tag = shift;

    $tag =~ s/ /%20/g;
    $tag =~ s/"/%22/g;

    return $tag;
}

sub process_message {
    my $cfg = shift;
    my $message = shift;

    my $command;
    my $taglist = [];

    $taglist = merge_existing_tags($cfg,$taglist,$message);
    $taglist = tag_list_id($cfg,$taglist,$message);
    $taglist = tag_folder_info($cfg,$taglist,$message);
    $taglist = tag_account_info($cfg,$taglist,$message);

    $taglist = tag_inbox($cfg,$taglist,$message);
    $taglist = remove_new_tags($cfg,$taglist,$message);

    # Remove Duplicate tags
    my %tags = map { $_ => 1 } @$taglist;
    @$taglist = keys %tags;
    @$taglist = map { encode_tag $_ } @$taglist;

    $command = join(' ', @{$taglist}) . " -- id:" . $message->{id} ;

    return $command;
}

sub main {
    my $cfg = shift;
    
    my @messages = get_messages( $cfg );
    my @tags;

    foreach my $message ( @messages ) {
        push @tags,process_message( $cfg, $message );
    }

    if ($cfg->{test}) {
        print join "\n",@tags;
    } else {
        open(my $nm, "|notmuch tag --batch") || die "can't fork: $!";
        local $SIG{PIPE} = sub { die "pipe broke" };
        my $tag;
        foreach $tag (@tags) {
            if ($cfg->{verbose}){
                print $tag . "\n";
            }
            print $nm $tag . "\n";
        }
        close $nm;
    }
}

my ($opt, $usage) = describe_options(
    'notperl %o <some-arg>',
    [ 'search|s=s' , "use a specific search instead of default tags" ],
    [ 'test|t'     , "Only output the actions to be preformed, don't run them." ],
    [ ],
    [ 'verbose|v'  , "print extra stuff" ],
    [ 'help|h'     , "print usage message and exit" ],
    );

my $cfg;

if (defined $ENV{HOME}) {
    $cfg = LoadFile("$ENV{HOME}/.notperl");
} else {
    print STDERR "Environment Variable HOME not defined";
    exit;
}

print($usage->text), exit if $opt->help;

$cfg->{maildir_len} = length($cfg->{maildir});
$cfg->{search} = $opt->search || "tag:$cfg->{newtag}";
$cfg->{test} = $opt->test || 0;
$cfg->{verbose} = $opt->verbose || 0;

main( $cfg );
    
__END__

=encoding UTF-8

=head1 notperl

A notmuch message tagging script

=cut
